=====================
 The Chocola website
=====================

This is the code of the web site for the Chocola_ seminar, built using the
Django_ web framework.

.. _Chocola: http://chocola.ens-lyon.fr/
.. _Django: https://www.djangoproject.com/

Deployment
==========

Host system
-----------

Install the following components on the host system:

- Python 3 with ``virtualenv``
- Apache with ``mod_wsgi``
- MySQL
- Gettext

Install required components to build the ``mysqlclient`` package for Python.
On Debian this means the packages ``gcc``, ``default-libmysqlclient-dev`` and
``python3-dev``.

Create the database, by issuing the following statements in ``mysql`` with
administrator rights::

  CREATE USER 'chocola'@'localhost' IDENTIFIED BY 'some_password';
  CREATE DATABASE chocola CHARACTER SET utf8;
  GRANT ALL on chocola.* TO 'chocola'@'localhost';

Configure Apache to allow virtual hosts. In ``/etc/apache2/ports.conf``, add::

  NameVirtualHost *:80


Code and Python dependencies
----------------------------

Clone the repository into ``/var/www/chocola`` and set it up::

  $ virtualenv -p python3 virtualenv
  $ . virtualenv/bin/activate
  $ pip install -r requirements.txt -r config/prod/requirements.txt
  $ ./manage.py migrate
  $ ./manage.py collectstatic
  $ ./manage.py compilemessages

Install the site in Apache. For the plain HTTP version::

  # ln -s /var/www/chocola/config/prod/apache.conf /etc/apache2/sites-available/chocola.conf
  # a2ensite chocola

For the HTTPS version::

  # ln -s /var/www/chocola/config/prod/apache-redirect.conf /etc/apache2/sites-available/chocola.conf
  # a2ensite chocola-redirect
  # ln -s /var/www/chocola/config/prod/apache-le-ssl.conf /etc/apache2/sites-available/chocola-le-ssl.conf
  # a2ensite chocola-le-ssl


SSL certificates
----------------

The SSL configuration uses a `Let's Encrypt`_ certificate.
For generating a certificate the first time, as explained by `Julien Anne`_,
do as follows.

.. _Let's Encrypt: https://letsencrypt.org/
.. _Julien Anne: https://news.julien-anne.fr/installer-et-configurer-lets-encrypt-pour-votre-application-django-sur-une-debian10/

- Start with the HTTP variant.

- Install certbot::

  # aptitude install python3-certbot-apache

- Deactivate WSGI in by commenting the WSGI directives in
  ``conf/prod/apache.conf``.

- Create a certificate with ``certbot`` (the ``LANG=C`` is because there is a
  UnicodeDecodeError when using an UTF8 locale)::

  LANG=C certbot --apache -d chocola.ens-lyon.fr

- Fix the locations of Apache configuration files, because certbot assumes
  they live in ``/etc/apache/sites-available`` while our setup links to
  ``/var/www/chocola/conf/prod``.
