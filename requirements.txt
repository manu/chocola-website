Django>=4.2,<5
django-ical
django-markdown-deux
django-markitup
django-registration
pypdf
reportlab>=3.6
-e git+https://framagit.org/manu/django-seminar.git#egg=django-seminar
