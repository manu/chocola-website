from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from django.views.static import serve
from seminar.views import event_next

urlpatterns = [
    path('media/<path:path>', serve, {'document_root': settings.MEDIA_ROOT}),
    path('admin/', admin.site.urls),
    path('accounts/', include('django_registration.backends.activation.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('infos/', include('infos.urls')),
    path('events/', include('seminar.urls')),
    path('', event_next, {'template_name': 'frontpage.html'}, name='frontpage'),
    path('acces.html', RedirectView.as_view(permanent=True,
        url='http://www.ens-lyon.fr/LIP/index.php/useful-informations#Venir'),
        name='acces'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
