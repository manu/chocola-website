# coding: utf-8
from django import forms
from django.forms.widgets import FileInput
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from infos.models import *


class RibWidget (FileInput):

    def render(self, name, value, attrs=None, renderer=None):
        template = u"%(value)s%(input)s<br/>" + \
            _(u"The file must be in PDF format.")
        subst = {'input': super(RibWidget, self).render(
            name, None, attrs=attrs, renderer=renderer)}
        if value is None or value == '':
            subst['value'] = u""
        else:
            subst['value'] = _(
                u"A bank ID was already provided.<br/>Replace it: ")
        return mark_safe(template % subst)


class UserForm (forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for key in ('first_name', 'last_name'):
            self.fields[key].required = True


class UserInformationFormBase (forms.ModelForm):

    class Meta:
        model = UserInformation
        fields = (
            'username', 'first_name', 'last_name', 'email',
            'position', 'institution', 'work_address',
            'birth_date', 'birth_place', 'nationality',
            'personal_address',
            'bank_name', 'bank_address', 'iban', 'rib',
        )
        widgets = {
            'rib': RibWidget,
        }

    username = forms.CharField(
        label=_(u"Username"),
        help_text=_(
            u"Required. 30 characters or fewer. "
            u"Letters, numbers and @/./+/-/_ characters"))
    first_name = forms.CharField(label=_(u"First name"))
    last_name = forms.CharField(label=_(u"Last name"))
    email = forms.EmailField(label=_(u"E-mail address"))

    def clean_username(self):
        username = self.cleaned_data['username']
        query = User.objects.filter(username=username)
        if query.count() != 0:
            user = query.get()
            if user.id != self.user_instance.id:
                raise ValidationError(_(u"This username is already used."))
        return username

    def save(self, commit=True):
        instance = super(UserInformationFormBase, self).save(commit=False)
        instance.user = self.user_instance
        for key in 'username', 'first_name', 'last_name', 'email':
            setattr(instance.user, key, self.cleaned_data[key])
        if commit:
            instance.user.save()
            instance.save()
        return instance


def userinformationform_factory(user):
    class UserInformationForm (UserInformationFormBase):
        user_instance = user
    return UserInformationForm


class RegistrationFormBase (forms.ModelForm):
    funding = forms.ModelChoiceField(
        label=_(u"Funding"),
        help_text=Registration.funding.field.help_text,
        queryset=Funding.objects.filter(active=True),
        required=False,
        empty_label=_(u"none"))

    field_groups = (
        {'title': "",
         'fields': ('userinfo', 'event', 'funding')},
        {'title': _(u"Funding"),
         'fields': ('cost')},
        {'title': _(u"Outward trip"),
         'fields': (
             'outward_from', 'outward_from_date', 'outward_from_time',
             'outward_to', 'outward_to_time')},
        {'title': _(u"Return trip"),
         'fields': (
             'return_from', 'return_from_date', 'return_from_time',
             'return_to', 'return_to_time')},
    )

    optional_fields = (
        'cost',
        'outward_from',
        'outward_from_date',
        'outward_from_time',
        'outward_to',
        'outward_to_time',
        'return_from',
        'return_from_date',
        'return_from_time',
        'return_to',
        'return_to_time',
    )

    def clean(self):
        cleaned_data = super(RegistrationFormBase, self).clean()
        funding = cleaned_data['funding']
        if funding is not None and funding.need_info:
            missing = cleaned_data['userinfo'].missing_fields()
            if missing:
                raise ValidationError(_(
                    u"To ask for this funding, you must provide "
                    u"all the required administrative info. "
                    u"Missing info: %s.") % u", ".join(missing))
            for field in self.optional_fields:
                if field not in cleaned_data or cleaned_data[field] is None or cleaned_data[field] == "":
                    raise ValidationError(_(
                        u"You must provide complete information "
                        u"about your trip."))
        return cleaned_data


class RegistrationForm (RegistrationFormBase):

    class Meta:
        model = Registration
        exclude = []
        widgets = {
            'event': forms.HiddenInput,
            'userinfo': forms.HiddenInput,
        }


class UserRegistrationForm (RegistrationFormBase):

    class Meta:
        model = Registration
        exclude = []
        widgets = {'event': forms.HiddenInput}
