from django.contrib.auth.decorators import *
from django.views.generic import *
from django.urls import re_path, reverse
from django.utils.functional import lazy

from infos.models import *
from infos.forms import *
from infos.views import *

reverse_lazy = lazy(reverse, str)

urlpatterns = [
    re_path(r'^perso/$',
        login_required(TemplateView.as_view(
            template_name='infos/perso.html')),
        name='infos_perso'),
    re_path(r'^perso/create/$',
        login_required(ProfileCreateView.as_view()),
        name='infos_create_profile'),
    re_path(r'^perso/edit/$',
        login_required(ProfileUpdateView.as_view()),
        name='infos_edit_profile'),
    re_path(r'^perso/registration/(?P<event_id>[0-9]+)/$',
        UserRegistrationView.as_view(),
        name='infos_user_registration'),
    re_path(r'^perso/registration/(?P<event_id>[0-9]+)/create/$',
        UserRegistrationCreateView.as_view(),
        name='infos_create_user_registration'),
    re_path(r'^perso/registration/(?P<event_id>[0-9]+)/edit/$',
        UserRegistrationUpdateView.as_view(),
        name='infos_edit_user_registration'),
    re_path(r'^perso/registration/(?P<event_id>[0-9]+)/delete/$',
        UserRegistrationDeleteView.as_view(),
        name='infos_delete_user_registration'),
    re_path(r'^user/(?P<username>[-\w._@]+)/$',
        permission_required('infos.view_info')(ProfileDetailView.as_view()),
        name='infos_user_detail'),
    re_path(r'^user/(?P<username>[-\w._@]+)/info.pdf$',
        user_info_as_pdf,
        name='infos_user_pdf'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/$',
        AdminRegistrationListView.as_view(),
        name='infos_event_registration'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/participants.csv$',
        registration_list_as_csv,
        name='infos_event_registration_csv'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/create/$',
        AdminRegistrationCreateView.as_view(),
        name='infos_registration_new'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/user/'
        r'(?P<username>[-\w._@]+)/$',
        AdminRegistrationView.as_view(),
        name='infos_registration'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/user/'
        r'(?P<username>[-\w._@]+)/create/$',
        AdminRegistrationCreateView.as_view(),
        name='infos_create_registration'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/user/'
        r'(?P<username>[-\w._@]+)/edit/$',
        AdminRegistrationUpdateView.as_view(),
        name='infos_edit_registration'),
    re_path(r'^registration/(?P<event_id>[0-9]+)/user/'
        r'(?P<username>[-\w._@]+)/delete/$',
        AdminRegistrationDeleteView.as_view(),
        name='infos_delete_registration'),
]
