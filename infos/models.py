# coding: utf-8
import os.path
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import pre_save, post_save, post_delete
from django.db.models import *
from django.dispatch import receiver
from django.forms import ValidationError
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from pypdf import PdfReader
from pypdf.errors import PyPdfError

from seminar.models import Event


def rib_file_name(instance, filename):
    return os.path.join('RIBs', instance.user.username, filename)


class PDFFileField (FileField):
    """
    A file field that validates its data as proper PDF.
    """

    def clean(self, *args, **kwargs):
        data = super(PDFFileField, self).clean(*args, **kwargs)
        try:
            PdfReader(data.file)
        except PyPdfError:
            raise ValidationError(_(u"Invalid PDF file"))
        data.content_type = 'application/pdf'
        return data


class UserInformation (Model):
    """
    Contains all required information for funding missions.
    """
    user = OneToOneField(User, CASCADE, related_name="profile")
    registrations = ManyToManyField(Event, through='Registration')

    birth_date = DateField(_(u"birth date"), blank=True, null=True)
    birth_place = CharField(_(u"birth place"), blank=True, max_length=80)
    nationality = CharField(_(u"nationality"), blank=True, max_length=80)
    personal_address = TextField(_(u"personal address"), blank=True)
    position = CharField(_(u"position"), blank=True, max_length=80)
    institution = CharField(_(u"institution"), max_length=80)
    work_address = TextField(_(u"work address"), blank=True)
    bank_name = CharField(_(u"bank name"), blank=True, max_length=80)
    bank_address = TextField(_(u"bank address"), blank=True)
    iban = CharField(_(u"IBAN code"), blank=True, max_length=80)
    rib = PDFFileField(_(u"bank ID"), blank=True, upload_to=rib_file_name)

    class Meta:
        ordering = ['user__last_name', 'user__first_name']
        permissions = (
            ('view_info', u"Can view all user information"),
        )

    def __str__(self):
        return self.user.get_full_name()

    def missing_fields(self):
        """
        Enumerate the fields where information is missing.
        """
        missing = []
        for field in (
                'birth_date', 'birth_place', 'nationality',
                'personal_address', 'position', 'institution', 'work_address',
                'bank_name', 'bank_address', 'iban', 'rib'):
            value = getattr(self, field)
            if value is None or value == '':
                missing.append(str(self._meta.get_field(field).verbose_name))
        return missing


class Funding (Model):
    """
    Represents a way of funding someone's participation.
    """
    name = CharField(
        max_length=80,
        help_text=_(u"A short description of the funding"))
    need_info = BooleanField(
        default=False,
        help_text=_(u"Whether this funding requires administrative info"))
    notified = ManyToManyField(
        User,
        blank=True,
        help_text=_(
            u"People to notify when someone asks for this funding"))
    active = BooleanField(
        default=True,
        help_text=_(
            u"Whether this funding is available in the registration form"))

    def __str__(self):
        return self.name


class Registration (Model):
    """
    Represents the subscription of one person to one event.
    """
    userinfo = ForeignKey(UserInformation, CASCADE,
                          help_text=_(u"The person to register"))
    event = ForeignKey(Event, CASCADE,
                       help_text=_(u"The event to register to"))
    funding = ForeignKey(Funding, CASCADE, null=True,
                         limit_choices_to={'active': True},
                         verbose_name=_(u"funding"),
                         help_text=_(u"The type of funding"))

    cost = FloatField(
        null=True, blank=True,
        verbose_name=_(u"cost"),
        help_text=_(u"Estimated cost for the participation (in euros)"))

    outward_from = CharField(max_length=80, blank=True,
                             help_text=_(u"Station of departure"))
    outward_from_date = DateField(blank=True, null=True,
                                  help_text=_(u"Date of departure"))
    outward_from_time = TimeField(null=True, blank=True,
                                  help_text=_(u"Time of departure"))
    outward_to = CharField(max_length=80, blank=True,
                           help_text=_(u"Station of arrival"))
    outward_to_time = TimeField(null=True, blank=True,
                                help_text=_(u"Time of arrival"))
    return_from = CharField(max_length=80, blank=True,
                            help_text=_(u"Station of departure"))
    return_from_date = DateField(blank=True, null=True,
                                 help_text=_(u"Date of departure"))
    return_from_time = TimeField(null=True, blank=True,
                                 help_text=_(u"Time of departure"))
    return_to = CharField(max_length=80, blank=True,
                          help_text=_(u"Station of arrival"))
    return_to_time = TimeField(null=True, blank=True,
                               help_text=_(u"Time of arrival"))

    @property
    def user(self):
        return self.userinfo.user

    class Meta:
        ordering = ['userinfo']
        unique_together = ('userinfo', 'event')

    def __str__(self):
        return u'<Registration: %s, %s>' % (
            self.userinfo.user.get_full_name(), self.event)

    def get_absolute_url(self):
        return reverse('infos_registration', kwargs={
                'event_id': self.event.id,
                'username': self.userinfo.user.username})

    def unique_error_message(self, model_class, unique_check):
        return _(u"There is already a registration for this person.")


def get_notified_users(funding):
    if funding:
        users = funding.notified.all()
    else:
        users = User.objects.none()
    group = Group.objects.filter(name='Gestion')
    if len(group) > 0:
        users = users | group.get().user_set.all()
    return users


@receiver(pre_save, sender=Registration)
def on_registration_save(sender, **kwargs):
    if kwargs.get('raw', False):
        return
    after = kwargs['instance']
    users = get_notified_users(after.funding)
    context = {'reg': after}
    if after.id:
        before = Registration.objects.get(id=after.id)
        context['before'] = before
        if before.funding != after.funding:
            if before.funding:
                users = users | before.funding.notified.all()
            context['funding_changed'] = True
        if before.cost != after.cost:
            context['cost_changed'] = True
            for key in (
                    'outward_from', 'outward_from_date', 'outward_from_time',
                    'outward_to', 'outward_to_time'):
                if getattr(before, key) != getattr(after, key):
                    context['outward_changed'] = True
                    break
            for key in (
                    'return_from', 'return_from_date', 'return_from_time',
                    'return_to', 'return_to_time'):
                if getattr(before, key) != getattr(after, key):
                    context['return_changed'] = True
                    break
        template = 'infos/registration_update_email.txt'
    else:
        template = 'infos/registration_create_email.txt'

    subject = u"[Chocola] inscription"
    text = render_to_string(template, context)
    to = list(set([u.email for u in users]))
    send_mail(subject, text, settings.DEFAULT_FROM_EMAIL, to)


@receiver(post_delete, sender=Registration)
def on_registration_deleted(sender, **kwargs):
    after = kwargs['instance']
    users = get_notified_users(after.funding)
    context = {'reg': after}
    template = 'infos/registration_delete_email.txt'

    subject = u"[Chocola] annulation"
    text = render_to_string(template, context)
    to = list(set([u.email for u in users]))
    send_mail(subject, text, settings.DEFAULT_FROM_EMAIL, to)
