from django.contrib import admin, auth
from django.contrib.auth.models import User

from infos.models import *


class ProfileInline (admin.StackedInline):
    model = UserInformation


class UserAdmin (auth.admin.UserAdmin):
    inlines = [ProfileInline]


class RegistrationAdmin (admin.ModelAdmin):
    model = Registration


class FundingAdmin (admin.ModelAdmin):
    model = Funding

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Registration, RegistrationAdmin)
admin.site.register(Funding, FundingAdmin)
