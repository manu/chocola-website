# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import infos.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('seminar', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Funding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='A short description of the funding', max_length=80)),
                ('need_info', models.BooleanField(default=False, help_text='Whether this funding requires administrative info')),
                ('active', models.BooleanField(default=True, help_text='Whether this funding is available in the registration form')),
                ('notified', models.ManyToManyField(help_text='People to notify when someone asks for this funding', to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cost', models.FloatField(help_text='Estimated cost for the participation (in euros)', null=True, verbose_name='cost', blank=True)),
                ('outward_from', models.CharField(help_text='Station of departure', max_length=80, blank=True)),
                ('outward_from_time', models.TimeField(help_text='Time of departure', null=True, blank=True)),
                ('outward_to', models.CharField(help_text='Station of arrival', max_length=80, blank=True)),
                ('outward_to_time', models.TimeField(help_text='Time of arrival', null=True, blank=True)),
                ('return_from', models.CharField(help_text='Station of departure', max_length=80, blank=True)),
                ('return_from_time', models.TimeField(help_text='Time of departure', null=True, blank=True)),
                ('return_to', models.CharField(help_text='Station of arrival', max_length=80, blank=True)),
                ('return_to_time', models.TimeField(help_text='Time of arrival', null=True, blank=True)),
                ('event', models.ForeignKey(on_delete=models.CASCADE, help_text='The event to register to', to='seminar.Event')),
                ('funding', models.ForeignKey(on_delete=models.CASCADE, verbose_name='funding', to='infos.Funding', help_text='The type of funding', null=True)),
            ],
            options={
                'ordering': ['userinfo'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserInformation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('birth_date', models.DateField(null=True, verbose_name='birth date', blank=True)),
                ('birth_place', models.CharField(max_length=80, verbose_name='birth place', blank=True)),
                ('insee_number', models.CharField(max_length=21, verbose_name='INSEE number', blank=True)),
                ('personal_address', models.TextField(verbose_name='personal address', blank=True)),
                ('position', models.CharField(max_length=80, verbose_name='position', blank=True)),
                ('institution', models.CharField(max_length=80, verbose_name='institution')),
                ('work_address', models.TextField(verbose_name='work address', blank=True)),
                ('bank_name', models.CharField(max_length=80, verbose_name='bank name', blank=True)),
                ('bank_address', models.TextField(verbose_name='bank address', blank=True)),
                ('iban', models.CharField(max_length=80, verbose_name='IBAN code', blank=True)),
                ('rib', infos.models.PDFFileField(upload_to=infos.models.rib_file_name, verbose_name='bank ID', blank=True)),
                ('registrations', models.ManyToManyField(to='seminar.Event', through='infos.Registration')),
                ('user', models.ForeignKey(on_delete=models.CASCADE, to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
                'ordering': ['user__last_name', 'user__first_name'],
                'permissions': (('view_info', 'Can view all user information'),),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='registration',
            name='userinfo',
            field=models.ForeignKey(on_delete=models.CASCADE, help_text='The person to register', to='infos.UserInformation'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='registration',
            unique_together=set([('userinfo', 'event')]),
        ),
    ]
