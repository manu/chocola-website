# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('infos', '0002_upgrade'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='outward_from_date',
            field=models.DateField(help_text='Date of departure', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='registration',
            name='return_from_date',
            field=models.DateField(help_text='Date of departure', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userinformation',
            name='nationality',
            field=models.CharField(max_length=80, verbose_name='nationality', blank=True),
        ),
        migrations.AlterField(
            model_name='userinformation',
            name='user',
            field=models.OneToOneField(on_delete=models.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL),
        ),
    ]
