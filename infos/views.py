# coding: UTF-8

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import *
from django.contrib.auth.models import User
from django.http import *
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.defaultfilters import date as _date
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic import *

from io import BytesIO
from reportlab.platypus import *
from reportlab.lib.styles import ParagraphStyle
from pypdf import PdfReader, PdfWriter
from pypdf.errors import PyPdfError

import csv

from seminar.models import Event
from infos.models import *
from infos.forms import *

# Profile management views


class ProfileMixin (object):
    model = UserInformation
    context_object_name = 'profile'

    def get_queryset(self):
        return UserInformation.objects.filter(user=self.user)

    def get_object(self):
        return self.get_queryset().get()

    def get_initial(self):
        initial = super(ProfileMixin, self).get_initial()
        for key in 'username', 'first_name', 'last_name', 'email':
            initial[key] = getattr(self.user, key)
        return initial

    def get_form_class(self):
        return userinformationform_factory(self.user)

    def get_success_url(self):
        return reverse('infos_perso')

    def dispatch(self, request, *args, **kwargs):
        if 'username' in kwargs:
            self.user = get_object_or_404(User, username=kwargs['username'])
        elif request.user.is_anonymous:
            self.user = None
        else:
            self.user = request.user
        return super(ProfileMixin, self).dispatch(request, *args, **kwargs)


class ProfileDetailView (ProfileMixin, DetailView):
    pass


class ProfileCreateView (ProfileMixin, CreateView):
    pass


class ProfileUpdateView (ProfileMixin, UpdateView):
    pass


class RegistrationMixin (object):
    """
    This mixin provides the common logic for all registration views, based on
    the standard generic views for objects. Querysets depend on an event,
    specified by an 'event_id' in the URL.
    """
    model = Registration
    form_class = RegistrationForm

    def get_queryset(self):
        return Registration.objects.filter(event=self.event)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            obj = queryset.filter(userinfo=self.userinfo).get()
        except ObjectDoesNotExist:
            obj = self.object_not_found(queryset)
        return obj

    def object_not_found(self, queryset):
        raise Http404(_(u"No %(verbose_name)s found matching the query") %
                      {'verbose_name': queryset.model._meta.verbose_name})

    def get_context_data(self, **kwargs):
        context = super(RegistrationMixin, self).get_context_data(**kwargs)
        context['event'] = self.event
        context['userinfo'] = self.userinfo
        context['funding_set'] = Funding.objects.filter(active=True)
        context['fundings_with_cost'] = Funding.objects.filter(
            active=True, need_info=True)
        return context

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(Event, id=kwargs['event_id'])
        response = self.set_userinfo(request, args, kwargs)
        if response is not None:
            return response
        if not self.check_perms(request):
            from django.contrib.auth.views import redirect_to_login
            from django.contrib.auth import REDIRECT_FIELD_NAME
            return redirect_to_login(
                request.get_full_path(), None, REDIRECT_FIELD_NAME)
        return super(
            RegistrationMixin, self).dispatch(request, *args, **kwargs)

    def check_perms(self, request):
        return True


class AdminRegistrationMixin (RegistrationMixin):
    """
    This mixin specializes access to registrations for administrators. This
    includes specifying the username in the URL and checking for permissions.
    """

    def set_userinfo(self, request, args, kwargs):
        if 'username' not in kwargs:
            self.userinfo = None
            return None
        self.userinfo = get_object_or_404(UserInformation,
                                          user__username=kwargs['username'])
        return None

    def check_perms(self, request):
        return request.user.has_perm('infos.view_info')

    def get_success_url(self):
        if self.userinfo is None:
            return reverse('infos_event_registration', kwargs={
                'event_id': self.event.id})
        return reverse('infos_registration', kwargs={
            'event_id': self.event.id,
            'username': self.userinfo.user.username})


class UserRegistrationMixin (RegistrationMixin):
    """
    This mixin specializes access to registration for users, by specifying the
    user as that of the request, assuming they are authenticated.
    """

    def set_userinfo(self, request, args, kwargs):
        if request.user.is_anonymous:
            return HttpResponseRedirect('%s?next=%s' % (
                reverse('login'), request.path))
        try:
            self.userinfo = UserInformation.objects.get(user=request.user)
            return None
        except UserInformation.DoesNotExist:
            return HttpResponseRedirect(reverse('infos_create_profile'))

    def check_perms(self, request):
        return request.user.is_authenticated

    def get_success_url(self):
        return reverse('infos_user_registration',
                       kwargs={'event_id': self.event.id})


class AdminRegistrationListView (AdminRegistrationMixin, ListView):

    def check_perms(self, request):
        return True


class AdminRegistrationView (AdminRegistrationMixin, DetailView):

    def object_not_found(self, queryset):
        self.template_name = 'infos/registration_missing.html'
        return None


class AdminRegistrationCreateView (AdminRegistrationMixin, CreateView):

    def get_initial(self):
        initial = super(AdminRegistrationCreateView, self).get_initial()
        initial['event'] = self.event
        initial['userinfo'] = self.userinfo
        return initial

    def get_form_class(self):
        if self.userinfo is None:
            return UserRegistrationForm
        else:
            return RegistrationForm


class AdminRegistrationUpdateView (AdminRegistrationMixin, UpdateView):
    pass


class AdminRegistrationDeleteView (AdminRegistrationMixin, DeleteView):
    pass


class UserRegistrationView (UserRegistrationMixin, DetailView):
    template_name = 'infos/user_registration_detail.html'

    def object_not_found(self, queryset):
        self.template_name = 'infos/user_registration_missing.html'
        return None


class UserRegistrationCreateView (UserRegistrationMixin, CreateView):
    template_name = 'infos/user_registration_form.html'

    def get_initial(self):
        initial = super(UserRegistrationCreateView, self).get_initial()
        initial['event'] = self.event
        initial['userinfo'] = self.userinfo
        initial['outward_to'] = "Lyon"
        initial['return_from'] = "Lyon"
        if self.event is not None:
            initial['outward_from_date'] = self.event.date_begin
            initial['return_from_date'] = self.event.date_end
        return initial


class UserRegistrationUpdateView (UserRegistrationMixin, UpdateView):
    template_name = 'infos/user_registration_form.html'


class UserRegistrationDeleteView (UserRegistrationMixin, DeleteView):
    template_name = 'infos/user_registration_confirm_delete.html'


@permission_required('infos.view_info')
def registration_list_as_csv(request, event_id):
    event = get_object_or_404(Event, id=event_id)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=participants.csv'

    writer = csv.writer(response, dialect=csv.excel)

    writer.writerow([_("last name"), _("first name"), _("funded by"), _("cost")])
    for registration in Registration.objects.filter(event=event):
        user = registration.userinfo.user
        row = [user.last_name, user.first_name]
        if registration.funding:
            row.extend([registration.funding.name, registration.cost])
        else:
            row.extend(['', ''])
        writer.writerow(row)

    return response


def preformatted(text, style):
    text = text.strip()
    if len(text) == 0:
        return None
    return Preformatted(text.replace('\r\n', '\n'), style)


@permission_required('infos.view_info')
def user_info_as_pdf(request, username):
    user = get_object_or_404(User, username=username)
    try:
        profile = user.profile
    except ObjectDoesNotExist:
        raise Http404

    style_normal = ParagraphStyle(
        'Normal',
        fontName='Helvetica',
        fontSize=10,
    )
    style_head = ParagraphStyle(
        'Head',
        parent=style_normal,
        fontName='Helvetica-Bold',
        fontSize=12,
        spaceBefore=10,
    )

    info_data = [
        [Paragraph(u"Identité", style_head)],
        [u"Nom:", profile.user.last_name],
        [u"Prénom:", profile.user.first_name],
        [u"Adresse électronique:", profile.user.email],
        [u"Date de naissance:", _date(profile.birth_date, 'd F Y')],
        [u"Lieu de naissance:", profile.birth_place],
        [u"Nationalité:", profile.nationality],
        [u"Adressse:", preformatted(profile.personal_address, style_normal)],
        [Paragraph(u"Informations professionnelles", style_head)],
        [u"Fonction et grade:", profile.position],
        [u"Organisme d'origine:", profile.institution],
        [u"Adressse:", preformatted(profile.work_address, style_normal)],
        [Paragraph(u"Informations bancaires", style_head)],
        [u"Banque:", profile.bank_name],
        [u"Adressse:", preformatted(profile.bank_address, style_normal)],
        [u"IBAN:", profile.iban],
    ]

    if profile.rib is None:
        rib = None
        info_data.append([u"RIB:", u"non disponible"])
    else:
        try:
            rib = PdfReader(profile.rib.file)
            info_data.append([u"RIB:", u"voir page suivante"])
        except ValueError:
            rib = None
            info_data.append([u"RIB:", u"non disponible"])
        except IOError:
            rib = None
            info_data.append([u"RIB:", u"non disponible"])
        except PyPdfError:
            rib = None
            info_data.append([u"RIB:", u"le fichier fourni est invalide"])

    info_table = Table(info_data, style=[
        ('SPAN', (0, 0), (1, 0)),
        ('SPAN', (0, 8), (1, 8)),
        ('SPAN', (0, 12), (1, 12)),
    ])

    buffer = BytesIO()
    doc = SimpleDocTemplate(buffer)
    doc.build([info_table])

    buffer.seek(0)
    front = PdfReader(buffer)
    out = PdfWriter()
    for page in front.pages:
        out.add_page(page)
    if rib is not None:
        for page in rib.pages:
            out.add_page(page)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % username
    out.write(response)
    return response
