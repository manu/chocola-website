#!/bin/bash
BASE=/var/www/chocola
source $BASE/virtualenv/bin/activate
cd $BASE
./manage.py clearsessions
mysqldump --user=chocola --password="$(cat /var/www/chocola/.dbpasswd)" chocola > backups/$(date +%Y%m%d).sql
