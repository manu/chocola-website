DEBUG = False

ADMINS = (
    ('Emmanuel Beffara', 'emmanuel.beffara@univ-grenoble-alpes.fr'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'chocola',
        'USER': 'chocola',
        'PASSWORD': open('/var/www/chocola/.dbpasswd').read().strip(),
        'HOST': 'localhost',
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['chocola.ens-lyon.fr', '140.77.166.88']
EMAIL_HOST = 'smtp-na.ens-lyon.fr'
DEFAULT_FROM_EMAIL = 'chocola-gestion@ens-lyon.fr'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
