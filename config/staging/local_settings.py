DEBUG = False

ADMINS = (
    ('Emmanuel Beffara', 'beffara@iml.univ-mrs.fr'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'chocola',
        'USER': 'chocola',
        'PASSWORD': file('/var/www/chocola/.dbpasswd').read().strip(),
        'HOST': 'localhost',
        'PORT': '',
    }
}

EMAIL_HOST = 'smtp.ens-lyon.fr'
DEFAULT_FROM_EMAIL = 'webmaster@chocola.ens-lyon.fr'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
